//********************************************************************************
/*!
\author     Kraemer Eduard
\date       05.04.2021

\file       HAL_Timer.c
\brief      Initialize the system timer. When an ARM-Core is available use the
            ARM-System Timer.

***********************************************************************************/

#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

#include <project.h>

#include "HAL_System.h"

#if defined(__GNUC__)
    #include <errno.h>
#endif

static u32 ulInterrupts[5];
static uint8_t ucEnterCritSession = 0;

//********************************************************************************
/*!
\author     Kraemer E.
\date       26.10.2020
\brief      Default handler for hard fault exceptions. Unfortunately no information
\param      none
\return     none
***********************************************************************************/
static void HardFaultISR(void)
{
    long error = errno;
    
    /* Make compiler happy */
    (void)error;

    while(1);
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       26.10.2020
\brief      Default handler for fault exceptions. Saves code, which can be read in "errno.h"
            and waits for WDT to occur
\param      none
\return     none
***********************************************************************************/
void CyBoot_IntDefaultHandler_Exception_EntryCallback(void)
{
    long error = errno;
    
    /* Make compiler happy */
    (void)error;
    
    while(1);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       12.04.2021
\brief      Reads the reset cause register and resets the cause bits.
            Returns the reset reason.
\return     eRstRsn - The reason for the occured reset.
\param      none
***********************************************************************************/
teResetReason HAL_System_GetResetReason(void)
{
    /* Check the Reset reason for a WDT reset */
    const u32 ulResetReason = CySysGetResetReason(CY_SYS_RESET_WDT | CY_SYS_RESET_PROTFAULT | CY_SYS_RESET_SW);
    
    teResetReason eRstRsn = eRstRsn_HardwareRst;
    
    //Hardware reset
    if(ulResetReason == CY_SYS_RESET_WDT)
    {
        eRstRsn = eRstRsn_WatchdogRst;
    }
    //Protection fault reset
    else if(ulResetReason == CY_SYS_RESET_PROTFAULT)
    {
        eRstRsn = eRstRsn_ProtectionFaultRst;
    }
    //Software reset
    else if(ulResetReason == CY_SYS_RESET_SW)
    {
        eRstRsn = eRstRsn_SoftwareRst;
    }
    
    return eRstRsn;    
}

//********************************************************************************
/*!
\author     Kraemer E
\date       12.05.2021
\brief      Initializes System depenedent fault interrupts.
\return     none
\param      none
***********************************************************************************/
void HAL_System_Init(void)
{
    //Switch Hard-fault interrupt handler to own method
    CyIntSetSysVector(CY_INT_HARD_FAULT_IRQN, HardFaultISR);
}

//********************************************************************************
/*!
\author     Kraemer E
\date       12.05.2021
\brief      Halts the CPU
\return     none
\param      ucReason - Value to be used during debugging
***********************************************************************************/
void HAL_System_HaltCPU(uint8_t ucReason)
{
    CyHalt(ucReason);
}

//********************************************************************************
/*!
\author     Kraemer E
\date       12.05.2021
\brief      Requests a software reset from the Core. Reset cause register will
            set bit in software reset reason.
\return     none
\param      none
***********************************************************************************/
void HAL_System_SoftwareReset(void)
{
    CySoftwareReset();
}

//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Just a delay for the given time interval. Timebase is millisecond.
\return     none
\param      ulDelayMs - Delay in milliseconds
***********************************************************************************/
void HAL_System_Delay(u32 ulDelayMs)
{
    CyDelay(ulDelayMs);
}

//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Enters a critical section by blocking interrupts
\return     none
\param      none
***********************************************************************************/
void HAL_System_EnterCriticalSection(void)
{
    ulInterrupts[ucEnterCritSession] = CyEnterCriticalSection();
    ucEnterCritSession++;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Leaves a critical section by unblocking interrupts
\return     none
\param      none
***********************************************************************************/
void HAL_System_LeaveCriticalSection(void)
{
    ucEnterCritSession--;
    CyExitCriticalSection(ulInterrupts[ucEnterCritSession]);
}

#endif //PSOC_4100S_PLUS