//********************************************************************************
/*!
\author     Kraemer Eduard
\date       05.04.2021

\file       HAL_Timer.c
\brief      Initialize the system timer. When an ARM-Core is available use the
            ARM-System Timer.

***********************************************************************************/

#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266


#include "HAL_System.h"
#include "ESP8266WiFi.h"

//********************************************************************************
/*!
\author     Kraemer E
\date       24.06.2021
\brief      Reads the reset reason and returns the specified reset-reason value.
\return     eRstRsn - The reason for the occured reset.
\param      none
***********************************************************************************/
teResetReason HAL_System_GetResetReason(void)
{
    /* Check the Reset reason for a WDT reset */
    const rst_info* psResetInfo = ESP.getResetInfoPtr();    
    teResetReason eRstRsn = eRstRsn_HardwareRst;
    
    switch (psResetInfo->reason)
    {
        case REASON_DEFAULT_RST:        // normal startup by power on        
        case REASON_DEEP_SLEEP_AWAKE:   // wake up from deep-sleep        
        case REASON_EXT_SYS_RST:        // external system reset
        {
            eRstRsn = eRstRsn_HardwareRst;     
               break;
        }

        case REASON_SOFT_WDT_RST:   // software watch dog reset, GPIO status won’t change        
        case REASON_WDT_RST:        // hardware watch dog reset
        {
            eRstRsn = eRstRsn_WatchdogRst;
            break;
        }

        case REASON_EXCEPTION_RST:  // exception reset, GPIO status won’t change
        {
            eRstRsn = eRstRsn_ProtectionFaultRst;
            break;
        }

        case REASON_SOFT_RESTART:   // software restart ,system_restart , GPIO status won’t change
        {
            eRstRsn = eRstRsn_SoftwareRst;    
            break;
        }

        default:
            break;
    }
    
    return eRstRsn;    
}

//********************************************************************************
/*!
\author     Kraemer E
\date       12.05.2021
\brief      Initializes System depenedent fault interrupts.
\return     none
\param      none
***********************************************************************************/
void HAL_System_Init(void)
{
    //Currently nothing to do
}

//********************************************************************************
/*!
\author     Kraemer E
\date       12.05.2021
\brief      Halts the CPU
\return     none
\param      ucReason - Value to be used during debugging
***********************************************************************************/
void HAL_System_HaltCPU(u8 ucReason)
{
    //Not available on ESP
}

//********************************************************************************
/*!
\author     Kraemer E
\date       12.05.2021
\brief      Requests a software reset from the Core. Reset cause register will
            set bit in software reset reason.
\return     none
\param      none
***********************************************************************************/
void HAL_System_SoftwareReset(void)
{
    ESP.restart();
}

//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Just a delay for the given time interval. Timebase is millisecond.
\return     none
\param      ulDelayMs - Delay in milliseconds
***********************************************************************************/
void HAL_System_Delay(u32 ulDelayMs)
{
    delay(ulDelayMs);
}

//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Enters the deep sleep state
\return     none
\param      none
***********************************************************************************/
void HAL_System_EnterDeepsleep(void)
{
    //Enter deep sleep forever 
    ESP.deepSleep(0);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Enters a critical section by blocking interrupts
\return     none
\param      none
***********************************************************************************/
void HAL_System_EnterCriticalSection(void)
{
    noInterrupts();
}

//********************************************************************************
/*!
\author     Kraemer E
\date       14.05.2021
\brief      Leaves a critical section by unblocking interrupts
\return     none
\param      none
***********************************************************************************/
void HAL_System_LeaveCriticalSection(void)
{
    interrupts();
}

#endif //ESPRESSIF_ESP8266