//********************************************************************************
/*!
\author     Kraemer Eduard
\date       05.04.2021

\file       HAL_SYSTEM.h
\brief      Interface for the system specific handling like HARD-Fault ISR

***********************************************************************************/


#ifndef _HAL_SYSTEM_H_
#define _HAL_SYSTEM_H_

#include "BaseTypes.h"
    
#ifdef __cplusplus
extern "C"
{
#endif

typedef enum
{
    eRstRsn_HardwareRst,        /* When a Power-on reset, a brownout reset or an external reset occurs no bits in the reset-register are set */
    eRstRsn_WatchdogRst,        /* Watchdog reset has occured. bit in RES_CAUSE register stays until cleared or by HardwareRst */
    eRstRsn_SoftwareRst,        /* Software reset has occured. bit in RES_CAUSE register stays until cleared or by HardwareRst */
    eRstRsn_ProtectionFaultRst  /* Unauthorized protection violations caused a device reset.  bit in RES_CAUSE register stays until cleared or by HardwareRst */    
}teResetReason;


void    HAL_System_Init(void);
teResetReason HAL_System_GetResetReason(void);
void    HAL_System_HaltCPU(u8 ucReason);
void    HAL_System_SoftwareReset(void);
void    HAL_System_Delay(u32 ulDelayMs);
void    HAL_System_EnterDeepsleep(void);

void    HAL_System_EnterCriticalSection(void);
void    HAL_System_LeaveCriticalSection(void);

#ifdef __cplusplus
}
#endif

#endif // _HAL_SYSTEM_H_
